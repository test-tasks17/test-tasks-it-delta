import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './app.css'
import Modal from './Modal';
import Header from './Header';
import BeatLoader from "react-spinners/BeatLoader";

function App() {
  const [images, setImages] = useState([]);
  const [loading, setLoading] = useState(true)
  const [modalActive, setModalActive] = useState(false);
  const [imgId, setImgId] = useState();

  useEffect(() => {
    axios.get('https://boiling-refuge-66454.herokuapp.com/images')
      .then(function (response) {
        setImages(response.data);
        setLoading(false)
      })
      .catch(function (error) {
        console.log('Error get img in main page', error);
      })
  }, []);


  return (
    <div className="container">
      <Header/>
      {
        loading ? <BeatLoader color="MediumSlateBlue" size={50} /> :
          <div className='content'>
            {images.map((img) => (
              <div className='img-container' onClick={() => {
                  setModalActive(true);
                  setImgId(img.id)
                }
              }>
                <img src={img.url} alt="icons" />
                <div className="img-id">id: {img.id}</div>
              </div>
            ))} {modalActive && <Modal active={modalActive} setActive={setModalActive} imgId={imgId} />}
            
            
          </div>

      }
    </div >
  );
}

export default App;
