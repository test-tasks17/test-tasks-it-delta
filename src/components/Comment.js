import React from 'react';
import './comment.css';
import user from '../assets/main/user.png'


const Comment = ({ text }) => {
  console.log(text);
  return (
    <div className='comment__content'>
      <img src={user} />
      <div>
        <div className='comment__name'>User</div>
        <div className='comment__text'>{text}</div>
      </div>
    </div>
  );
};

export default Comment;