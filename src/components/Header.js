import React from 'react';
import './header.css';
import title from '../assets/main/title.png'
import chuvak from '../assets/main/chuvak.jpg'


const Header = () => {

  return (
    <div className='title__content'>
      <div className='title__img'>
        <img src={title} ></img>
      </div>
      <div className='title__bottom-content'>
        <>
          <div className='title__contact'>
            <img src={chuvak} ></img>
          </div>
          <div className='title__name'>Denis Demin</div>
        </>
        <div className='title__button-content'>
        <a href="mailto:demka_dd@bk.ru"><button><i className='icon-message'/>Message</button></a>
        <a href="tel:+790013838888"> <button><i className='icon-call'/>Call</button> </a>
        </div>
      </div>
    </div>
  );
};

export default Header;