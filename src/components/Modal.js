import React, { useEffect, useState } from 'react';
import axios from 'axios';
import qs from 'qs';
import './modal.css';
import Comment from './Comment';
import BeatLoader from "react-spinners/BeatLoader";

const Modal = ({ active, setActive, imgId }) => {
  const [bigImage, setBigImage] = useState();
  const [bigImageId, setBigImageId] = useState();
  const [comment, setComment] = useState('');
  const [loading, setLoading] = useState(false);

  const postComment = (comment, bigImageId) => {
    axios({
      method: 'post',
      url: `https://boiling-refuge-66454.herokuapp.com/images/${bigImageId}/comments`,
      data: qs.stringify({
        name: 'User',
        comment: { comment }
      }),
      headers: {
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      }
    })
      .then(function (response) {
        setLoading(false);
        console.log(response);
      })
      .catch(function (error) {
        console.log('Error post comment', error);
      })
  }
  useEffect(() => {
    if (imgId) {
      axios.get(`https://boiling-refuge-66454.herokuapp.com/images/${imgId}`)
        .then(function (response) {
          setBigImage(response.data);
          setBigImageId(imgId);

        })
        .catch(function (error) {
          console.log('Error get img in modal', error);
        })
    }
  }, [imgId]);
  return (
    <div className={active ? "modal active" : "modal"} onClick={() => setActive(false)}>
      <div className='modal__content' onClick={(e) => e.stopPropagation()}>
        {bigImage && bigImageId === imgId &&
          <img src={bigImage.url} alt="Big"></img>
        }
        <div className='modal__comments-block'>
          {bigImage?.comments.length > 0 ? bigImage.comments.map((item) => <Comment text={item.text} />)
            : <div className='comment-not-found'>No one has left a comment on this photo yet </div>}

        </div>
        <form className='modal__form' onSubmit={(e) => { postComment(comment, bigImageId); e.preventDefault(); setLoading(true); setComment('')}}>
          <div className='modal__comment'>
            Comment
          </div>
          <textarea className='modal__input' value={comment} onChange={(e) => setComment(e.target.value)} />
          <div className='modal__down-text'>
          Write a few sentences about the photo.
          </div>
          <input className='modal__button' type="submit" value="Save" />
          {loading && <BeatLoader color="MediumSlateBlue" size={20} />}
        </form>


      </div>

    </div>
  );
};

export default Modal;